new Vue({
    el: '#datepicker',
    data: {
      selectedDate: new Date(),
      attrs: [
        {
          key: 'today',
          highlight: true,
          dates: new Date(),
        }]
    },
});