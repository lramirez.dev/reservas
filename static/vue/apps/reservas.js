new Vue({
    el: '#app',
    delimiters: ['{$','$}'],
    data: {
        usuario: {
            'nombreUsuario': '',
            'estadoUsuario': 0,
        },
        usuariosResActiva: [],
        usuariosResInactiva: [],
        conteoPorUsuario: [],
        conteoActivoPorUsuario: [],
        conteoInactivoPorUsuario: [],
        reserva: {
            'usuarioReserva': 0,
            'fechaReserva': null,
            'fechaAtencion': null,
            'estadoReserva': 0,
        },
        selectUser: false,
        attrs: [
            {
              key: 'today',
              highlight: true,
              dates: new Date(),
              timezone: '',
        }],
        modelConfig: {
            type: 'string',
            mask: 'DD-MM-YYYY',
        },
    },
    beforeMount(){
        
    },
    mounted(){
        this.updateAll();
    },
    beforeUpdate(){
    },
    methods: {

        async conteoReservasUsuario(){
            await axios.get("http://127.0.0.1:8000/api/reservasConteo/")
            .then(respuesta=>this.conteoPorUsuario=respuesta.data.results)
        },

        async conteoReservasUsuarioActivas(){
            await axios.get("http://127.0.0.1:8000/api/reservasActivasConteo/")
            .then(respuesta=>this.conteoActivoPorUsuario=respuesta.data.results)
        },

        async conteoReservasUsuarioInactivas(){
            await axios.get("http://127.0.0.1:8000/api/reservasInactivasConteo/")
            .then(respuesta=>this.conteoInactivoPorUsuario=respuesta.data.results)
        },

        async listaUsuariosResActiva(){
            await axios.get("http://localhost:8000/api/reservaSearch/?estado=0")
            .then(respuesta=>this.usuariosResActiva=respuesta.data.results)
        },

        async listaUsuariosResInactiva(){
            await axios.get("http://localhost:8000/api/reservaSearch/?estado=1")
            .then(respuesta=>this.usuariosResInactiva=respuesta.data.results)
        },

        async creaUsuario(){
            await axios.post("http://localhost:8000/api/usuarios/", this.usuario)
            .then((result) => {
                console.log(result);
            }).catch(function(error){
                console.log(error);
            });
            this.resetInput();
            this.updateAll();
        },

        async creaReserva(){
            await axios.post("http://localhost:8000/api/reservas/", this.reserva)
            .then((result) => {
                console.log(result);
            }).catch(function(error){
                console.log(error);
            });
            this.resetInput();
            this.updateAll();
            this.selectUser = false;
        },

        async deleteUsuario(id){
            await axios.delete("http://localhost:8000/api/usuarios/"+id).then((result) => {
                console.log(result);
            }).catch(function(error){
                console.log(error);
            });
            this.updateAll();
        },

        async deleteReserva(id, usuario){
            await axios.delete("http://localhost:8000/api/reservas/"+id).then((result) => {
                console.log(result);
            }).catch(function(error){
                console.log(error);
            });
            this.updateAll();
        },

        async confirmaAtencion(id, usuario, estado){

            if(estado == 0){
                estado = 1;
            }
            else{
                estado = 0;
            }

            await axios.put("http://localhost:8000/api/reservas/"+id+"/",{
                    'usuarioReserva': usuario,
                    'estadoReserva': estado,
                })
                .then((result) => {
                    console.log(result);
                }).catch(function(error){
                    console.log(error);
                });
                
            this.updateAll();
        },

        async resetInput() {
            this.usuario.nombreUsuario = "";
        },

        selectUsuario(id) {
            this.reserva.usuarioReserva = id
            if(this.selectUser==false){
                this.selectUser = true;
            }
            else{
                this.selectUser = false;
            }
        },

        selectFecha(fecha) {
            this.reserva.fechaReserva = fecha.id;
            if(this.selectUser){
                document.getElementById("myModal").style.display = "block";
            }
            
        },

        updateAll(){
            this.listaUsuariosResActiva();
            this.listaUsuariosResInactiva();
            this.conteoReservasUsuario();
            this.conteoReservasUsuarioActivas();
            this.conteoReservasUsuarioInactivas();
        },


    }
});