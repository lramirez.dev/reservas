from django.shortcuts import render
from rest_framework import viewsets, permissions
from .serializers import ReservaListaSerializer, UsuarioListaSerializer, ReservaSerializer, ConteoReservasSerializer, ConteoReservasActivasSerializer, ConteoReservasInactivasSerializer
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView

from rest_framework.filters import SearchFilter, OrderingFilter
from django.db.models import Count, Q




# Llamado a modelos necesarios
from appreservas.models import Reserva, Usuario

class ReservaListaJson(viewsets.ModelViewSet):
    #permission_classes = [IsAuthenticatedOrReadOnly]
    permission_classes = [AllowAny]
    queryset = Reserva.objects.all().order_by('fechaReserva')
    serializer_class = ReservaListaSerializer

class UsuarioListaJson(viewsets.ModelViewSet):
    #permission_classes = [IsAuthenticatedOrReadOnly]
    permission_classes = [AllowAny]
    queryset = Usuario.objects.all().order_by('estadoUsuario','nombreUsuario')
    serializer_class = UsuarioListaSerializer

#class ReservaListView(viewsets.ModelViewSet):
#    queryset = Reserva.objects.all()
#    serializer_class = ReservaSerializer
    #authentication_classes = (TokenAuthentication,)
    #permission_classes = (IsAuthenticated,)
    #pagination_class = PageNumberPagination
#    permission_classes = [AllowAny]
#    filter_backends = (SearchFilter, OrderingFilter,)
#    search_fields = ('=estadoReserva', '=usuarioReserva__nombreUsuario')


class ReservaListView(viewsets.ModelViewSet):
    serializer_class = ReservaSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        queryset = Reserva.objects.all()
        usuario = self.request.query_params.get('usuario', None)
        estado = self.request.query_params.get('estado', None)
        if usuario is not None and estado is not None:
            queryset = queryset.filter(usuarioReserva=usuario, estadoReserva=estado)
        elif usuario is not None:
            queryset = queryset.filter(usuarioReserva=usuario)
        elif estado is not None:
            queryset = queryset.filter(estadoReserva=estado).order_by('fechaReserva')
        #else:
            #queryset = Reserva.objects.values('usuarioReserva').annotate(dcount=Count('usuarioReserva'))
            
        return queryset

class ConteoReservasView(viewsets.ModelViewSet):
    queryset = Usuario.objects.annotate(conteo=Count('usuarioReserva'))
    serializer_class = ConteoReservasSerializer

class ConteoReservasActivasView(viewsets.ModelViewSet):
    queryset = Usuario.objects.annotate(conteo=Count('usuarioReserva', filter=Q(usuarioReserva__estadoReserva=0)))
    serializer_class = ConteoReservasActivasSerializer

class ConteoReservasInactivasView(viewsets.ModelViewSet):
    queryset = Usuario.objects.annotate(conteo=Count('usuarioReserva', filter=Q(usuarioReserva__estadoReserva=1)))
    serializer_class = ConteoReservasInactivasSerializer



    