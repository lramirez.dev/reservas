from rest_framework import serializers

# Llamado a modelos necesarios
from appreservas.models import Reserva, Usuario

class UsuarioListaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        #fields = ['nombreUsuario', 'estadoUsuario']
        fields = "__all__"

class ReservaListaSerializer(serializers.ModelSerializer):
    usuario = UsuarioListaSerializer(read_only=True, many=False)
    class Meta:
        model = Reserva
        #fields = ['id', 'usuarioReserva', 'fechaReserva', 'fechaAtencion', 'estadoReserva', 'usuario' ]
        fields = "__all__"

class ReservaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reserva
        #fields = ['usuarioReserva', 'fechaReserva', 'fechaAtencion', 'estadoReserva']
        fields = "__all__"

class ConteoReservasSerializer(serializers.ModelSerializer):
    conteo = serializers.IntegerField()
    class Meta:
        model = Usuario
        fields = ['nombreUsuario', 'conteo']
        #fields = "__all__"

class ConteoReservasActivasSerializer(serializers.ModelSerializer):
    conteo = serializers.IntegerField()
    class Meta:
        model = Usuario
        fields = ['nombreUsuario', 'conteo']
        #fields = "__all__"

class ConteoReservasInactivasSerializer(serializers.ModelSerializer):
    conteo = serializers.IntegerField()
    class Meta:
        model = Usuario
        fields = ['nombreUsuario', 'conteo']
        #fields = "__all__"