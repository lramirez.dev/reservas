from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'usuarios', views.UsuarioListaJson, basename='Usuario')
router.register(r'reservas', views.ReservaListaJson, basename='Reservas')
router.register(r'reservaSearch', views.ReservaListView, basename='ReservaSearch')
router.register(r'reservasConteo', views.ConteoReservasView, basename='ReservasConteo')
router.register(r'reservasActivasConteo', views.ConteoReservasActivasView, basename='ReservasActivas')
router.register(r'reservasInactivasConteo', views.ConteoReservasInactivasView, basename='ReservasInactivas')

urlpatterns = [
    path('', include(router.urls)),
]
