from django.contrib import admin
from .models import Reserva, Usuario

admin.site.register(Reserva)
admin.site.register(Usuario)