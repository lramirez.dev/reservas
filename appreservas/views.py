from django.shortcuts import render
from .models import Reserva
from django.conf import settings

def inicio(request):
    reservas = Reserva.objects.all().order_by('fechaReserva')
    return render(request, 'appreservas/index.html', {'reservas': reservas})
