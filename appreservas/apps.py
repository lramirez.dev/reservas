from django.apps import AppConfig


class AppreservasConfig(AppConfig):
    name = 'appreservas'
