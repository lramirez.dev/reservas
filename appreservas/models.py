from django.db import models
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericRelation

# Estado de la reserva
ESTADO_RESERVA_CHOICES = (
    (0,'Pendiente'),
    (1, 'Atendido'),
)

# Estado de Usuario
ESTADO_USUARIO_CHOICES = (
    (0,'Sin reserva'),
    (1, 'Con Reserva Activa'),
    (2, 'Con Reserva Inactiva'),
)

class Reserva(models.Model):
    usuarioReserva = models.ForeignKey('Usuario', related_name="usuarioReserva",on_delete=models.CASCADE)
    fechaReserva = models.DateField(default=timezone.now)
    fechaAtencion = models.DateField(null=True, default='null')
    estadoReserva = models.IntegerField(default=0, choices=ESTADO_RESERVA_CHOICES)

    def __str__(self):
        return str(self.fechaReserva)

class Usuario(models.Model):
    nombreUsuario = models.CharField(max_length=100, primary_key=True)
    estadoUsuario = models.IntegerField(default=0, choices=ESTADO_USUARIO_CHOICES)

    def __str__(self):
        return self.nombreUsuario